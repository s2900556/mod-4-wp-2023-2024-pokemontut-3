package nl.utwente.mod4.pokemon;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.util.JSONPObject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.MediaType;
import nl.utwente.mod4.pokemon.dao.PokemonDao;
import nl.utwente.mod4.pokemon.dao.PokemonTypeDao;
import nl.utwente.mod4.pokemon.model.PokemonType;
import nl.utwente.mod4.pokemon.model.ResourceCollection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@ApplicationPath("/api")
public class PokeApp extends Application {

}