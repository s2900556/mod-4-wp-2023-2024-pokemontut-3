package nl.utwente.mod4.pokemon.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.NotSupportedException;
import nl.utwente.mod4.pokemon.Utils;
import nl.utwente.mod4.pokemon.model.PokemonType;

import java.io.File;
import java.io.IOException;
import java.util.*;

public enum PokemonDao {

    INSTANCE;

    private static final String ORIGINAL_POKEMON = Utils.getAbsolutePathToResources() + "/aggregated-and-filtered-pokemon-dataset.json";

    private Map<String, PokemonType> pokemons = new HashMap<>();
    public void load() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        File source = new File(ORIGINAL_POKEMON);
        PokemonType[] arr = mapper.readValue(source, PokemonType[].class);

        Arrays.stream(arr).forEach(pt -> pokemons.put(pt.id, pt));
    }


    public List<Object> getNames() {
        List<PokemonType> list = new ArrayList<>(pokemons.values());
        List<Object> names = new ArrayList<>();
        for (PokemonType p : list){
            if (p.name != null) {
                names.add(p.name);
            }
        }
        return names;
    }

    public Object getPokemon(String id){
        if (pokemons.containsKey(id)) {
            return pokemons.get(id).name;
        } else {
            return new NotFoundException().toString();
        }
    }

}
