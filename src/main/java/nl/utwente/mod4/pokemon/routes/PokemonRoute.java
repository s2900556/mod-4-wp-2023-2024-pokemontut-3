package nl.utwente.mod4.pokemon.routes;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import nl.utwente.mod4.pokemon.dao.PokemonDao;
import nl.utwente.mod4.pokemon.dao.PokemonTypeDao;
import nl.utwente.mod4.pokemon.model.NamedEntity;
import nl.utwente.mod4.pokemon.model.PokemonType;
import nl.utwente.mod4.pokemon.model.ResourceCollection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Path("/pokemon")
public class PokemonRoute {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Object> getPokemonTypes(
            @QueryParam("sortBy") String sortBy,
            @QueryParam("pageSize") int pageSize,
            @QueryParam("pageNumber") int pageNumber
    ) throws IOException {
        int ps = pageSize > 0 ? pageSize : Integer.MAX_VALUE;
        int pn = pageNumber > 0 ? pageNumber : 1;
        PokemonDao.INSTANCE.load();
        var resources = PokemonDao.INSTANCE.getNames();

        return resources;

    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getPokemonName(@PathParam("id") String id) {
        return PokemonDao.INSTANCE.getPokemon(id);
    }
}
